# json2envar
A simple CLI to set your environment variables using a JSON file as the input.

### Install via 'go get' command:

``` bash
# Install
go get gitlab.com/hardyin/json2envar
```

### Usage:
``` bash
# set environment variables
json2envar set config.json

# unset environment variables (non-Windows only)
json2envar unset config.json
```

### Manual Install:
``` bash
git clone git@gitlab.com:hardyin/json2envar.git
cd json2envar
go install
```

## License
[MIT](https://choosealicense.com/licenses/mit/)