package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"os/user"
	"runtime"

	"github.com/urfave/cli"
)

var app = cli.NewApp()

func main() {
	info()
	commands()
	err := app.Run(os.Args)
	check(err)
}

func info() {
	app.Name = "json2envar"
	app.Usage = "A simple CLI to set your environment variables using a JSON file as the input"
	app.Author = "Hardyin Alexander"
	app.Version = "1.0.0"
}

func commands() {
	app.Commands = []cli.Command{
		{
			Name:    "set",
			Aliases: []string{"s"},
			Usage:   "json2envar set file.json",
			Action: func(c *cli.Context) {
				fileName := c.Args().Get(0)
				theMap := readJSON(fileName)

				if runtime.GOOS == "windows" {
					setEnvWindows(theMap)
				} else {
					setEnvLinux(theMap)
				}
			},
		},
		{
			Name:    "unset",
			Aliases: []string{"u"},
			Usage:   "json2envar unset file.json",
			Action: func(c *cli.Context) {
				fileName := c.Args().Get(0)
				theMap := readJSON(fileName)

				if runtime.GOOS == "windows" {
					fmt.Println("unfortunately this command is not avaliable in windows")
				} else {
					unsetEnvLinux(theMap)
				}
			},
		},
	}
}

func readJSON(fileName string) map[string]string {
	jsonFile, err := os.Open(fileName)
	check(err)
	fmt.Println("Successfully opened", fileName)

	defer jsonFile.Close()

	byteValue, _ := ioutil.ReadAll(jsonFile)

	var result map[string]string

	err = json.Unmarshal([]byte(byteValue), &result)
	check(err)

	return result
}

func setEnvWindows(theMap map[string]string) {

	keyword := "setx"

	for key, value := range theMap {
		cmd := exec.Command(keyword, key, value)
		fmt.Println(cmd)
		out, err := cmd.CombinedOutput()
		check(err)
		fmt.Printf(string(out))
	}
	fmt.Println("json2envar has succesfully set your environment variables")
}

func setEnvLinux(theMap map[string]string) {
	usr, err := user.Current()
	check(err)

	bashrc := usr.HomeDir + "/.bashrc"
	f, err := os.OpenFile(bashrc, os.O_APPEND|os.O_WRONLY, 0777)
	check(err)

	f.WriteString("# Commands below are appended by json2envar \n")
	for key, value := range theMap {
		toWrite := "export " + key + "=" + value + "\n"
		if _, err := f.WriteString(toWrite); err != nil {
			f.Close()
			panic(err)
		}
	}
	f.WriteString("# ========================================= \n")

	f.Close()
	exec.Command("source", bashrc)
	fmt.Println("json2envar has succesfully set your environment variables")

}

func unsetEnvLinux(theMap map[string]string) {
	usr, err := user.Current()
	check(err)

	bashrc := usr.HomeDir + "/.bashrc"
	f, err := os.OpenFile(bashrc, os.O_APPEND|os.O_WRONLY, 0777)
	check(err)

	f.WriteString("# Commands below are appended by json2envar \n")
	for key := range theMap {
		toWrite := "unset " + key + "\n"
		if _, err := f.WriteString(toWrite); err != nil {
			f.Close()
			panic(err)
		}
	}
	f.WriteString("# ========================================= \n")

	f.Close()
	exec.Command("source", bashrc)
	fmt.Println("json2envar has succesfully unset your environment variables")
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}
